package io.toro.jeraldjoe.taskNote.Controller;


import io.toro.jeraldjoe.taskNote.Model.NoteModel;
import io.toro.jeraldjoe.taskNote.Services.NoteService;
import io.toro.jeraldjoe.taskNote.utils.NoteUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
public class NoteController {

    private final NoteService noteService;

    @Autowired
    public NoteController(NoteService noteService) {
        this.noteService = noteService;
    }

    @PostMapping("/note")
    public NoteModel createNewNote(@Valid @RequestBody NoteModel noteModel) {
        return noteService.createNote(noteModel);
    }

    @GetMapping("/note")
    public List<NoteModel> listAllNote(){
        return noteService.listAll();
    }

    @GetMapping("/note/filter")
    public List<NoteModel> listAllSortedNote(@RequestParam("sort") String sortBy){
        return noteService.sortAllNotes(sortBy);
    }


    @GetMapping("/note/find")
    public List<NoteModel> searchByNote(@RequestParam("search") String search){
        List<NoteModel> nModel= noteService.listAll();
        return  NoteUtils.SearchInImportanceCreateDateTitle(nModel,search);
    }

    @GetMapping("/note/{noteId}")
    public ResponseEntity<NoteModel> listOneNote(@PathVariable("noteId") Long noteId){
        return noteService.findOneNote(noteId);
    }

    @PutMapping("/note/{noteId}")
    public ResponseEntity<NoteModel> updateNote(@PathVariable("noteId") Long noteId,@Valid @RequestBody NoteModel updatenoteModel){
        return noteService.updateNote(noteId,updatenoteModel);
    }


    @DeleteMapping("/note/{noteId}")
    public ResponseEntity<NoteModel> deleteNote(@PathVariable("noteId") Long noteId){
        return noteService.deleteNote(noteId);
    }

}
