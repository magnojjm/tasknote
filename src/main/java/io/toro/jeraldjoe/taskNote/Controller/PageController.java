package io.toro.jeraldjoe.taskNote.Controller;

import io.toro.jeraldjoe.taskNote.Model.NoteForm;
import io.toro.jeraldjoe.taskNote.Model.NoteModel;
import io.toro.jeraldjoe.taskNote.Services.NoteService;
import io.toro.jeraldjoe.taskNote.utils.NoteUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class PageController {

    private final NoteService noteService;
    private NoteForm noteForm = new NoteForm();
    @Autowired
    public PageController(NoteService noteService) {
        this.noteService = noteService;
    }



    @GetMapping("/index")
    public String index(Model model){
        model.addAttribute("note", noteService.listAll());
        model.addAttribute("noteForm", noteForm);
        return "index";
    }

    @PostMapping(value = "/index")
    public String addNotes(Model model,@ModelAttribute("noteForm") NoteForm noteform){

        String title = noteform.getNoteTitle(),
               content = noteform.getNoteContent(),
               importance = noteform.getIsNoteImportant();
        MultipartFile pictureToBeUpload = noteform.getUploadedPicture();

        noteService.insertNote(title,content,importance,pictureToBeUpload);

        return "redirect:/index";
}

    @GetMapping("/index/delete/{noteId}")
    public String deleteNotes(@PathVariable("noteId") Long noteId, Model model){
        model.addAttribute("noteForm",noteForm);
        noteService.deleteNote(noteId);
        return "redirect:/index";
    }

    @GetMapping("/index/find")
    public String findNotes(@RequestParam("search") String searchWord, Model model){
        List<NoteModel> nModel= noteService.listAll();
        model.addAttribute("note", NoteUtils.SearchInImportanceCreateDateTitle(nModel,searchWord));
        model.addAttribute("noteForm", noteForm);
        return "/index";
    }

    @GetMapping("/index/filter")
    public String sortNotes(@RequestParam("sort") String sortKeyword, Model model){
        model.addAttribute("note", noteService.sortAllNotes(sortKeyword));
        model.addAttribute("noteForm", noteForm);
        return "/index";
    }

}
