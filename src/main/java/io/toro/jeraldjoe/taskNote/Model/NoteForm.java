package io.toro.jeraldjoe.taskNote.Model;

import org.springframework.web.multipart.MultipartFile;

public class NoteForm {

    private String noteTitle;
    private String noteContent;
    private String isNoteImportant;
    private MultipartFile uploadedPicture;

    public String getNoteTitle() {
        return noteTitle;
    }

    public String getNoteContent() {
        return noteContent;
    }

    public String getIsNoteImportant() {
        return isNoteImportant;
    }

    public MultipartFile getUploadedPicture() {
        return uploadedPicture;
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }

    public void setIsNoteImportant(String isNoteImportant) {
        this.isNoteImportant = isNoteImportant;
    }

    public void setUploadedPicture(MultipartFile uploadedPicture) {
        this.uploadedPicture = uploadedPicture;
    }
}
