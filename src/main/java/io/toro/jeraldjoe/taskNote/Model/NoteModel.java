package io.toro.jeraldjoe.taskNote.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.toro.jeraldjoe.taskNote.utils.NoteUtils;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.IOException;
import java.util.Date;

@Entity
@Table(name = "tbl_notes")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"noteCreateDate", "noteUpdateDate"},allowGetters =true)
public class NoteModel {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long noteId;

    @NotBlank
    @Column(columnDefinition = "Varchar(50)")
    private String noteTitle;

    @NotBlank
    @Column(columnDefinition = "Varchar(250)")
    private  String noteContent;

    @Column(columnDefinition = "longblob")
    private String encodedPicture;

    @Column(updatable = true)
    private Boolean isNoteImportant;

    @CreatedDate
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date noteCreateDate;

    @LastModifiedDate
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date noteUpdateDate;

    public void NoteModel(Long noteId,
                     String noteTitle,
                     String noteContent,
                     String encodedPicture,
                     Boolean isNoteImportant,
                     Date noteCreateDate,
                     Date noteUpdateDate) {
        this.noteId = noteId;
        this.noteTitle = noteTitle;
        this.noteContent = noteContent;
        this.encodedPicture = encodedPicture;
        this.isNoteImportant = isNoteImportant;
        this.noteCreateDate = noteCreateDate;
        this.noteUpdateDate = noteUpdateDate;
    }

    public Long getNoteId() {
        return noteId;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public String getNoteContent() {
        return noteContent;
    }

    public String getEncodedPicture(){
        return NoteUtils.dataURL(encodedPicture);
    }

    public String getNoteImportant() {
        return NoteUtils.importantOrNot(isNoteImportant);
    }

    public String getNoteCreateDate() {
        return NoteUtils.formatCreateAndUpdateDate(noteCreateDate);
    }

    public String getNoteUpdateDate() {
        return NoteUtils.formatCreateAndUpdateDate(noteUpdateDate);
    }

    public void setNoteTitle(String noteTitle) {
        this.noteTitle = noteTitle;
    }

    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }

    public void setEncodedPicture(String encodedPicture) {
        this.encodedPicture = encodedPicture;
    }

    public void setNoteImportant(Boolean noteImportant) {
        isNoteImportant = noteImportant;
    }
}
