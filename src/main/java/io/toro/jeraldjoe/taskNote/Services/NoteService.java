package io.toro.jeraldjoe.taskNote.Services;

import io.toro.jeraldjoe.taskNote.Model.NoteModel;
import io.toro.jeraldjoe.taskNote.Repository.NoteRepository;
import io.toro.jeraldjoe.taskNote.utils.NoteUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class NoteService {

    private final NoteRepository noteRepository;

    @Autowired
    public NoteService(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    public NoteModel createNote(NoteModel noteModel){
        return noteRepository.save(noteModel);
    }

    public NoteModel insertNote(String title, String content, String importance,MultipartFile pictureUpload){
        NoteModel nM = new NoteModel();
        nM.setNoteTitle(title);
        nM.setNoteContent(content);
        nM.setNoteImportant(Boolean.parseBoolean(importance));
        nM.setEncodedPicture(NoteUtils.encodeMultiPartPictureToBase64(pictureUpload));
        return noteRepository.save(nM);
    }


    public List<NoteModel> listAll(){
        return noteRepository.findAll();
    }

    public List<NoteModel> sortAllNotes(String sortBy){
        return sortBy.equalsIgnoreCase("important asc")? noteRepository.findAllByOrderByIsNoteImportantAscNoteCreateDate():
               sortBy.equalsIgnoreCase("important desc")? noteRepository.findAllByOrderByIsNoteImportantDescNoteCreateDate():
               sortBy.equalsIgnoreCase("datecreated asc") ? noteRepository.findAllByOrderByNoteCreateDateAsc() :
               sortBy.equalsIgnoreCase("datecreated desc") ? noteRepository.findAllByOrderByNoteCreateDateDesc():
               sortBy.equalsIgnoreCase("dateupdated asc") ? noteRepository.findAllByOrderByNoteUpdateDateAsc() :
               sortBy.equalsIgnoreCase("dateupdated desc") ? noteRepository.findAllByOrderByNoteUpdateDateDesc():
               noteRepository.findAll();
    }

    public ResponseEntity<NoteModel> findOneNote(Long noteId){
        NoteModel noteModel = noteRepository.findOne(noteId);

        if(noteModel == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(noteModel);
    }

    public ResponseEntity<NoteModel> updateNote(Long noteId, NoteModel updatenoteModel){
        NoteModel noteModel = noteRepository.findOne(noteId);

        if (noteModel==null){
            return ResponseEntity.notFound().build();
        }

        noteModel.setNoteTitle(updatenoteModel.getNoteTitle());
        noteModel.setNoteContent(updatenoteModel.getNoteContent());
        noteModel.setNoteImportant(Boolean.parseBoolean(updatenoteModel.getNoteImportant()));

        NoteModel newNoteModel = noteRepository.save(noteModel);

        return ResponseEntity.ok(newNoteModel);
    }

    public ResponseEntity<NoteModel> deleteNote(Long noteId){
        NoteModel noteModel = noteRepository.findOne(noteId);

        if (noteModel==null){
            return ResponseEntity.notFound().build();
        }

        noteRepository.delete(noteModel);
        return ResponseEntity.ok().build();
    }


}
