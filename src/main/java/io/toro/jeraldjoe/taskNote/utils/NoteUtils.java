package io.toro.jeraldjoe.taskNote.utils;

import io.toro.jeraldjoe.taskNote.Model.NoteModel;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.web.multipart.MultipartFile;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class NoteUtils {

    public static String formatCreateAndUpdateDate(Date inputDate) {
        return new SimpleDateFormat("MMM dd, yyyy  hh:mm a").format(inputDate);
    }


    public static String encodePathPictureToBase64(String path) {
        String encodedfile = null;
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(new File(path));
            byte[] bytes = new byte[(int)new File(path).length()];
            fileInputStreamReader.read(bytes);
            encodedfile = Base64.encodeBase64String(bytes);
            fileInputStreamReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error Converting Image");
        }
        return encodedfile;
    }

    public static String encodeMultiPartPictureToBase64(MultipartFile pic) {
        String encodedfile = null;
        try {
            byte[] bytes = pic.getBytes();
            encodedfile = Base64.encodeBase64String(bytes);
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error Converting Image");
        }
        return encodedfile;
    }


    public static List<NoteModel> SearchInImportanceCreateDateTitle(List<NoteModel> nModel, String searchWord) {
        return nModel.stream()
                .filter((a) -> a.getNoteImportant().toLowerCase().contains(searchWord.toLowerCase()) ||
                               a.getNoteCreateDate().toLowerCase().contains(searchWord.toLowerCase()) ||
                               a.getNoteTitle().toLowerCase().contains(searchWord.toLowerCase()))
                .collect(Collectors.toList());
    }

    public static String dataURL(String encodeImage){
        return encodeImage == "" ? " " :"data:image/png/jpeg;base64, "+encodeImage;
    }

    public static String importantOrNot(Boolean importance){
        return importance ? "Important Note" : "Not important";
    }
}


